<?php

//=============== Boolean to Integer
    $first = false;
    $first = intval($first);
    //$first = (int) $first;
    //$first = +$first;
    echo $first;
    echo PHP_EOL;

//=============== Integer to Boolean
//            Positive
    $secondPos = 10;
    $secondPos = (bool) $secondPos;
    echo ('Converted from positive int: ' . $secondPos);
    echo PHP_EOL;
//            Negative
    $secondNeg = -3;
    $secondNeg = (bool) $secondNeg;
    echo ('Converted from negative int: ' . $secondNeg);
    echo PHP_EOL;
//            Neutral
    $secondNull = 0;
    $secondNull = (bool) $secondNull;
    echo ('Converted from neutral int: ' . $secondNull);
    echo PHP_EOL;

//=============== Float to Integer
    $third = 7.8;
    $third = intval($third);
    echo $third;
    echo PHP_EOL;

//=============== Integer to Float
    $fourth = 10;
    $fourth = (float)$fourth + .5;
    echo $fourth;
    echo PHP_EOL;

//=============== String to Integer
    $fifth = '1st string variable in current lab';
    $fifth = (int) $fifth;
    echo $fifth, PHP_EOL;

//=============== Integer to String
    $sixth = -1280;
    $sixth = (string) $sixth;
    echo $sixth, PHP_EOL;
    $sixth2str = "$sixth";    //another way to convert str to int
    echo $sixth2str, PHP_EOL;

//=============== Array to String
    $myArr = array(5, 7, 85, 64);
    $arr2str = (string) $myArr;
    var_dump($arr2str);
    echo PHP_EOL;

//=============== String to Array
    $myStr = 'darkside';
    $str2arr = (array) $myStr;
    print_r($str2arr);
    echo PHP_EOL;
    $anotherArr = str_split($myStr);
    echo $anotherArr[4];

 exit;