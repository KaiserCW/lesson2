<?php

    $var = ' CUSTOM VARIABLE ';

//==================== Simple single-quoted string with variable
    $singleQuoted1 = 'Single-qouted string with ' . $var;
    echo $singleQuoted1, PHP_EOL;

//==================== Single-quoted string with characters escaping
    $singleQuoted2 = 'I\'m single-quoted string with \n characters escaping';
    echo $singleQuoted2, PHP_EOL;

//==================== Simple double-quoted string with variable
    $doubleQuoted1 = "Simple double-quoted string with $var";
    echo $doubleQuoted1, PHP_EOL;

//==================== Double-quoted string with escape sequances
    $doubleQuoted2 = "Double-quoted \$tring \$ays: \"I\t have\t a lot of\t escape\t sequances\"";
    echo $doubleQuoted2, PHP_EOL;

//==================== Simple single-quoted string with variable
    echo <<<'PIKACHU'
    quu..__
 $$$b  `---.__
  "$$b        `--.                          ___.---uuudP
   `$$b           `.__.------.__     __.---'      $$$$"              .
     "$b          -'            `-.-'            $$$"              .'|
       ".                                       d$"             _.'  |
         `.   /                              ..."             .'     |
           `./                           ..::-'            _.'       |
            /                         .:::-'            .-'         .'
           :                          ::''\          _.'            |
          .' .-.             .-.           `.      .'               |
          : /'$$|           .@"$\           `.   .'              _.-'
         .'|$u$$|          |$$,$$|           |  <            _.-'
         | `:$$:'          :$$$$$:           `.  `.       .-'
         :                  `"--'             |    `-.     \
        :##.       ==             .###.       `.      `.    `\
        |##:                      :###:        |        >     >
        |#'     `..'`..'          `###'        x:      /     /
         \                                   xXX|     /    ./
          \                                xXXX'|    /   ./
          /`-.                                  `.  /   /
         :    `-  ...........,                   | /  .'
         |         ``:::::::'       .            |<    `.
         |             ```          |           x| \ `.:``.
         |                         .'    /'   xXX|  `:`M`M':.
         |    |                    ;    /:' xXXX'|  -'MMMMM:'
         `.  .'                   :    /:'       |-'MMMM.-'
          |  |                   .'   /'        .'MMM.-'
          `'`'                   :  ,'          |MMM<
            |                     `'            |tbap\
             \                                  :MM.-'
              \                 |              .''
               \.               `.            /
                /     .:::::::.. :           /
               |     .:::::::::::`.         /
               |   .:::------------\       /
              /   .''               >::'  /
              `',:                 :    .'
    
    THIS IS POKEDOC SYNTAX!
PIKACHU;

exit;