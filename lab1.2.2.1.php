<?php

//INTEGER
$a = 7;
echo "\$a is integer, it's value = $a";
echo PHP_EOL;
echo '$a is integer, it\'s value = ' . $a;
echo PHP_EOL;

//BOOLEAN
$b = true;
echo '$b is boolean, it\'s value = ' . $b;
echo PHP_EOL;

//FLOAT
$c = 7.5;
echo '$c is float, it\'s value = ' . $c;
echo PHP_EOL;

//STRING
$d = 'My str';
echo '$d is string, it\'s value = ' . $d;
echo PHP_EOL;

$dec = 0b0000111;
echo '$dec is integer in decimal, it\'s value = ' . $dec;