<?php

//================= Integer + string of numbers
    $myVar1 = 48 + '855';
    echo $myVar1, PHP_EOL;

//================= String without numbers + Integer
    $myVar2 = 'I have no any numbers' + 75;
    echo $myVar2, PHP_EOL;

//================= String without numbers + String without numbers
    $myVar3 = 'I\'m string ' + 'without numbers';
    echo $myVar3, PHP_EOL;

//================= Float + Integer
    $myVar4 = 14.06 + 22;
    echo $myVar4, PHP_EOL;

//================= Compare sum of float numbs with expected result
    $result = (7.3 + 8.1 == 15.4);
    var_dump($result);
    echo 7.3 + 8.1 . ' - MYSTIC!';